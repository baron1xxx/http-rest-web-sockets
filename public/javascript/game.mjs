import {
    ERROR_ADD_USER,
    CREATE_ROOM,
    ERROR_ADD_ROOM,
    UPDATE_ROOMS,
    JOIN_ROOM,
    JOIN_ROOM_DONE,
    UPDATE_USERS_IN_ROOM,
    LEAVE_ROOM,
    CHANGE_READY_STATUS_USER,
    UPDATE_READY_STATUS
} from './constants/socketClientEvents.mjs'
import {createElement, addClass, removeClass} from './helper.mjs'

const username = sessionStorage.getItem("username");

// TODO add room and geme helper file

if (!username) {
    window.location.replace("/login");
}

const socket = io("http://localhost:3002/game", {query: {username}});

const buttonCreateRoom = document.getElementById("button-create-room");
const roomsContainer = document.getElementById("rooms-container");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const roomNameContainer = document.getElementById("room-name");
const leftStatusGameContainer = document.getElementsByClassName('left-status-game-container')[0];
const rightStatusGameContainer = document.getElementsByClassName('right-status-game-container')[0];
const usersContainer = document.getElementsByClassName('users-container')[0];
const readyButton = document.getElementById('is-ready-button');



socket.on(ERROR_ADD_USER, message => {
    alert(message);
    sessionStorage.clear();
    window.location.replace('/login');
});

socket.on(ERROR_ADD_ROOM, message => {
    alert(message);
});

buttonCreateRoom.addEventListener('click', () => {
    const roomName = prompt('Enter room name');
    socket.emit(CREATE_ROOM, roomName);
});

//**********************************************************************************************************************

// UPDATE ROOMS
const createRoomCard = room => {
    const roomCardContainer = createElement({
        tagName: "div",
        className: "room-container flex-centered-vertical"
    });
    const connectedUsers = createElement({
        tagName: "div",
        className: "room-item-connected-users"
    });
    const roomName = createElement({
        tagName: "div",
        className: "room-item-name"
    });
    const roomButton = createElement({
        tagName: "button",
        className: "button room-item-join"
    });

    const onJoinRoom = () => {
        socket.emit(JOIN_ROOM, room.name);
    };

    roomButton.addEventListener("click", onJoinRoom);

    connectedUsers.innerHTML = `${room.users.length} users connected`;
    roomName.innerHTML = `Room: # ${room.name}`;
    roomButton.innerHTML = 'Join';

    roomCardContainer.appendChild(connectedUsers);
    roomCardContainer.appendChild(roomName);
    roomCardContainer.appendChild(roomButton);

    return roomCardContainer;
};
const updateRooms = rooms => {
    const allRooms = rooms.filter(roomItem => {
        return roomItem.gameStarted === false && roomItem.users.length < roomItem.maxUsersForRoom
    }).map(createRoomCard);
    roomsContainer.innerHTML = ''; // clean rooms container
    roomsContainer.append(...allRooms);
};

const createRoomName = name => {
    const roomNameTag = createElement({tagName: 'h1', className: '', attributes: {id: 'room-name'}});
    roomNameTag.innerHTML = name;
    return roomNameTag
};

export const leaveRoom = () => {
    addClass(gamePage, 'display-none');
    removeClass(roomsPage, 'display-none');
};

const createLeaveRoomButton = (roomName) => {
    const leaveRoomButton = createElement({tagName: 'button', className: 'button', attributes: {id: 'leave-room'}});
    const onLeaveRoom = () => {
        socket.emit(LEAVE_ROOM, roomName);
        leaveRoom();
    };
    leaveRoomButton.addEventListener("click", onLeaveRoom);
    leaveRoomButton.innerHTML = 'Back';
    return leaveRoomButton
};

const createUserContainer = user => {
    const username = sessionStorage.getItem('username');

    const userContainer = createElement({tagName: 'div', className: '', attributes: {id: 'user'}});
    const userStatusBlock = createElement({tagName: 'div', className: 'user-status-block'});
    const userStatus = createElement({tagName: 'span', className: 'user-status bg-red', attributes: {id: 'user-status'}});
    const userNameTag = createElement({tagName: 'h2', className: '', attributes: {id: 'user-name'}});
    userNameTag.innerHTML = username === user.name ? `${user.name} (you)` : user.name;

    userStatusBlock.append(userStatus, userNameTag);

    const progressContainer = createElement({tagName: 'div', className: '', attributes: {id: 'progress-container'}});
    const progressBackground = createElement({tagName: 'div', className: 'progress-background'});
    progressContainer.append(progressBackground);

    user.isReady ? addClass(userStatus, 'bg-green') : removeClass(userStatus, 'bg-green');

    userContainer.append(userStatusBlock);
    userContainer.append(progressContainer);
    return userContainer;
};

const refreshUsersToRoom = (users) => {
    usersContainer.innerHTML = '';
    usersContainer.append(...users.map(user => createUserContainer(user)));
    leftStatusGameContainer.append(usersContainer);
};

const joinRoomDone = ({users, roomName}) => {
    addClass(roomsPage, 'display-none');
    removeClass(gamePage, 'display-none');
    const leftStatusGameContainer = document.getElementsByClassName('left-status-game-container')[0];
    const roomNameTag = createRoomName(roomName);
    const leaveRoomButton = createLeaveRoomButton(roomName);
    leftStatusGameContainer.append(roomNameTag);
    leftStatusGameContainer.append(leaveRoomButton);
    refreshUsersToRoom(users);
};

const changeReadyStatusUser = () => {
    const roomName = document.getElementById('room-name').innerText;
    socket.emit(CHANGE_READY_STATUS_USER, { userId: socket.id, roomName });
};

readyButton.addEventListener('click', changeReadyStatusUser);

const updateReadyStatus = readyStatus => {
    const readyStatusButton = document.getElementById('is-ready-button');
    readyStatusButton.innerHTML = readyStatus ? 'Not Ready' : 'Ready';
};


socket.on(UPDATE_ROOMS, updateRooms);
socket.on(JOIN_ROOM_DONE, joinRoomDone);
socket.on(UPDATE_USERS_IN_ROOM, refreshUsersToRoom);
socket.on(UPDATE_READY_STATUS, updateReadyStatus);
