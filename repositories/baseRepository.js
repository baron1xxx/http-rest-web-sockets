/**
 *
 * @type {{
 * rooms: [{
 *     name: string,
 *     users: [userId],
 *     gameStarted: boolean
 * }],
 * users: [{
 *      id: string (socket.id)
 *      name: string,
 *      isReady: boolean
 * }]}}
 */

const defaultDb = {users: [], rooms: []};

export default class BaseRepository {
    constructor(collectionName) {
        this.collectionName = collectionName;
        this.dbContext = defaultDb[collectionName];
    }


    getAll() {
        return this.dbContext;
    }

    getOne(search) {
        return this.dbContext.find(el => el.name === search);
    }

    getById(id) {
        return this.dbContext.find(el => el.id === id);
    }

    create(data) {
        const {id} = data;
        this.dbContext.push(data);
        return this.dbContext.find(el => el.id === id);
    }

    update(id, dataToUpdate) {

    }

    delete(id) {
        const findIndex = this.dbContext.findIndex(el => el.id === id);
        this.dbContext.splice(findIndex, 1);
    }

    deleteByName(name) {
        const findIndex = this.dbContext.findIndex(el => el.name === name);
        this.dbContext.splice(findIndex, 1);
    }
}
