import BaseRepository from './baseRepository'


class RoomRepository extends BaseRepository {

    addUserToRoom(roomName, userId) {
        this.getOne(roomName).users.push(userId);
    }

    deleteByUser(userId) {
        const findIndex = this.dbContext.findIndex(el => el.userId === userId);
        this.dbContext.splice(findIndex, 1);
    }

}

export default new RoomRepository('rooms');
