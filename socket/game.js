import * as config from './config'
import UserService from '../services/userService'
import RoomService from '../services/roomService'
import {
    ERROR_ADD_USER,
    CREATE_ROOM,
    ERROR_ADD_ROOM,
    UPDATE_ROOMS,
    JOIN_ROOM,
    JOIN_ROOM_DONE,
    UPDATE_USERS_IN_ROOM,
    LEAVE_ROOM,
    CHANGE_READY_STATUS_USER,
    UPDATE_READY_STATUS
} from '../constants/socketServerEvents'

export default io => {
    io.on("connection", socket => {
        console.log(`Connection - ${socket.id}`);

        const emitError = (eventName, message) => {
            socket.emit(eventName, message);
        };
        const username = socket.handshake.query.username;
        const {message} = UserService.create({id: socket.id, name: username, isReady: false});
        if (message) {
            emitError(ERROR_ADD_USER, message);
        }

        // UPDATE ROOMS
        io.emit(UPDATE_ROOMS, RoomService.getAll());


        // CREATE ROOM BY NAME
        socket.on(CREATE_ROOM, name => {
            const {message} = RoomService.create({
                name,
                userId: socket.id
            });
            if (message) {
                emitError(ERROR_ADD_ROOM, message);
            }
            io.emit(UPDATE_ROOMS, RoomService.getAll());
            const usersId = RoomService.getAll().find(el => el.name === name).users;
            const users = UserService.getAllById(usersId);
            socket.join(name, () => {
                io.to(socket.id).emit(JOIN_ROOM_DONE, {users, roomName: name});
            });
        });

        // JOIN ROOM
        socket.on(JOIN_ROOM, roomName => {
            RoomService.addUserToRoom(roomName, socket.id);
            const usersId = RoomService.search(roomName).users;
            const users = UserService.getAllById(usersId);
            socket.join(roomName, () => {
                io.to(socket.id).emit(JOIN_ROOM_DONE, {users, roomName});
                io.emit(UPDATE_ROOMS, RoomService.getAll());
                io.to(roomName).emit(UPDATE_USERS_IN_ROOM, UserService.getAllById(RoomService.search(roomName).users));
            });
        });

        // LEAVE ROOM
        socket.on(LEAVE_ROOM, roomName => {
            const usersRoom = RoomService.search(roomName).users;
            const userIndex = usersRoom.indexOf(socket.id);
            usersRoom.splice(userIndex, 1);
            socket.leave(roomName);
            if (!usersRoom.length) {
                RoomService.deleteByName(roomName);
            }
            io.emit(UPDATE_ROOMS, RoomService.getAll());
            io.to(roomName).emit(UPDATE_USERS_IN_ROOM, UserService.getAllById(RoomService.search(roomName).users));
        });

        // CHANGE READY STATUS USER
        socket.on(CHANGE_READY_STATUS_USER, ({userId, roomName}) => {
            const user = UserService.getById(userId);
            user.isReady = !user.isReady;

            socket.emit(UPDATE_READY_STATUS, user.isReady);
            io.emit(UPDATE_ROOMS, RoomService.getAll());
            io.to(roomName).emit(UPDATE_USERS_IN_ROOM, UserService.getAllById(RoomService.search(roomName).users));
        });

        socket.on("disconnect", () => {

            console.log(`Disconnect - ${socket.id}`);
            UserService.delete(socket.id);
            RoomService.deleteByUserId(socket.id);
            io.emit(UPDATE_ROOMS, RoomService.getAll());
        });
    });
};
