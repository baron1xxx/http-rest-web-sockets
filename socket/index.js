import login from "./login";
import game from "./game";


export default io => {
    login(io.of("/login"));
    game(io.of("/game"));
};
