import UserRepository from '../repositories/userRepository'

class UserService {

    getAll() {
        return UserRepository.getAll();
    }

    getAllById(usersId) {
        return usersId.map(userId => this.getById(userId));
    }

    search(search) {
        const user = UserRepository.getOne(search);
        if (!user) {
            return null;
        }
        return user;
    }

    getById(id) {
        return UserRepository.getById(id);
    }

    create(data) {
        const username = data.name.trim();
        if (!username || username === 'null' || !username.length) {
            return {message: 'Error!!! Username is empty or null'}
        }
        const userExists = this.search(username);
        return userExists
            ? {message: 'Error!!! User exists'}
            : UserRepository.create(data)
    }


    delete(id) {
        UserRepository.delete(id);
    }

}

export default new UserService();
