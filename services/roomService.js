import RoomRepository from '../repositories/roomRepository'
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../socket/config'

class RoomService {

    getAll() {
        return RoomRepository.getAll();
    }

    search(search) {
        const room = RoomRepository.getOne(search);
        if (!room) {
            return null;
        }
        return room;
    }

    create(data) {
        const { name = '', userId } = data;
        const roomName = name.trim();
        if (!roomName || roomName === 'null' || !roomName.length) {
            return {message: 'Error!!! Room name is empty or null'}
        }
        const roomExists = this.search(roomName);
        return roomExists
            ? {message: 'Error!!! Room exists'}
            : RoomRepository.create({
                name,
                users: [userId],
                gameStarted: false,
                maxUsersForRoom: MAXIMUM_USERS_FOR_ONE_ROOM,
            })
    }

    addUserToRoom(roomName, userId) {
        RoomRepository.addUserToRoom(roomName, userId)
    }


    delete(id) {
        RoomRepository.delete(id);
    }

    deleteByName(name) {
        RoomRepository.deleteByName(name);
    }

    deleteByUserId(id) {
        RoomRepository.deleteByUser(id);
    }

}

export default new RoomService();
